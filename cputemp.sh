#!/bin/bash
while :
do
	s1=$(cat /sys/devices/virtual/thermal/thermal_zone0/temp)
        s2=$(cat /sys/devices/virtual/thermal/thermal_zone1/temp)
        s3=$(cat /sys/devices/virtual/thermal/thermal_zone2/temp)
        s4=$(cat /sys/devices/virtual/thermal/thermal_zone3/temp)

        clear
        echo "SENSOR_1 - CPU"
	echo "scale=1;$s1/1000" | bc -l
        echo "SENSOR_2 - GPU" 
        echo "scale=1;$s2/1000" | bc -l
        echo "SENSOR_3 - VE"
        echo "scale=1;$s3/1000" | bc -l
        echo "SENSOR_4 - DDR" 
        echo "scale=1;$s4/1000" | bc -l
	sleep 1
done