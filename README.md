# Orangepizero2_3.0.6_ubuntu_jammy_server_linux5.16.17

A script I use for fresh installs of Orangepizero2_3.0.6_ubuntu_jammy_server_linux5.16.17 on my Orangepizero2.

Image taken from https://drive.google.com/drive/folders/1ohxfoxWJ0sv8yEHbrXL1Bu2RkBhuCMup.

**Changes**
1. sudo nmcli device modify eth0 ipv4.dns "1.1.1.1,1.0.0.1" #Change netplan-eth0 DNS to Cloudflare DNS 1.1.1.1 & 1.0.0.1
2. sudo nmcli device modify eth0 ipv4.ignore-auto-dns yes #Ignore default DNS for netplan-eth0
3. sudo nmcli -t -f IP4.DNS device show eth0 #Confirm DNS changes.
3. rm /etc/apt/trusted.gpg~ #Remove trusted.gpg
4. curl -o /etc/apt/sources.list https://assets.ahunt.uk/orangepizero2/sources.list #Update sources.
5. sudo apt-get update && sudo apt-get upgrade #Update packages from new source.

Old sources.list = https://assets.ahunt.uk/orangepizero2/old_sources.list

New sources.list = https://assets.ahunt.uk/orangepizero2/sources.list

cputemp.sh just prints out temps from all the sensors on the board in a loop.