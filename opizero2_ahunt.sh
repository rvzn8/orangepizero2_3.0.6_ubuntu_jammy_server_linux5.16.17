#!/bin/bash
sudo nmcli device modify eth0 ipv4.dns "1.1.1.1,1.0.0.1"
sudo nmcli device modify eth0 ipv4.ignore-auto-dns yes
sudo nmcli -t -f IP4.DNS device show eth0
rm /etc/apt/trusted.gpg~
curl -o /etc/apt/sources.list https://assets.ahunt.uk/orangepizero2/sources.list
sudo apt-get update && sudo apt-get upgrade